#!/usr/bin/env python3

import logging

from PyQt5 import QtCore, QtGui, QtWidgets
from layout import Ui_Dialog
from logger import Ui_Logger
from client_git import Client

__version__ = '1.1.2'
__author__  = 'Glemison C Dutra'
__created__ = '03/04/2021'

class Thread(QtCore.QThread):
    logger = QtCore.pyqtSignal(str)
    stopped   = QtCore.pyqtSignal(bool)

    def __init__(self, parent, client):
        super(Thread, self).__init__(parent)
        self.client = client
        self.client.logger = self.logger.emit

    def run(self):
        try:
            self.stopped.emit(False)
            if not self.client.email or not self.client.username:
                if self.client.is_first_login():
                    self.logger.emit('Error: No primeiro push, o email e usuario sao obrigatorios')
                    return
            self.client.init()
            self.client.append_all_files()
            self.client.commit()
            self.client.add_remote()
            self.client.push()
        except Exception as e:
            self.logger.emit(e)
        finally:
            self.stopped.emit(True)

class QTextEditLogger(logging.Handler):
    def __init__(self, logger_plain):
        super(QTextEditLogger, self).__init__()
        self.logger_plain = logger_plain

    def emit(self, record):
        msg = self.format(record)
        self.logger_plain.appendPlainText(msg)

class Logger(Ui_Logger, QtWidgets.QDialog):
    def __init__(self):
        super(Logger, self).__init__()
        self.setupUi(self)
        self.logTextBox = QTextEditLogger(self.logger_area)
        self.logTextBox.setFormatter(logging.Formatter('%(asctime)s- %(message)s', datefmt='%H:%M:%S'))
        logging.getLogger().addHandler(self.logTextBox)
        logging.getLogger().setLevel(logging.DEBUG)
        self.pushButton_back.clicked.connect(self.close)
        self.show_info()
        
    def clear_log(self):
        self.logger_area.clear()
        self.show_info()
        
    def show_info(self):
        logging.info('Por: ' + __author__)
        logging.info('Versao: ' + __version__)
        logging.info('Criado em: ' + __created__)

class App(QtWidgets.QStackedWidget, Ui_Dialog):
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)
        self.setupUi(self)
        self.pushButton_start.clicked.connect(self.open_process_git)
        self.pushButton_log.clicked.connect(self.open_logger)
        self.pushButton_open_dir.clicked.connect(self._open_file_dialog)
        self.pushButton_start.setEnabled(False)
        self.lineEdit_link.textChanged[str].connect(self.release_button)
        self.dialog_logger = Logger()
    
    def release_button(self, text):
        if len(text) > 10 and self.lineEdit_directory.text():
            self.pushButton_start.setEnabled(True)
        else:
            self.pushButton_start.setEnabled(False)

    def _open_file_dialog(self):
        directory = str(QtWidgets.QFileDialog.getExistingDirectory())
        self.lineEdit_directory.setText('{}'.format(directory))
        if self.pushButton_start.isEnabled() is False and self.lineEdit_link.text():
            self.pushButton_start.setEnabled(True)
    
    def logger(self, text):
        logging.info(text)
    
    def open_logger(self):
        self.addWidget(self.dialog_logger)
        self.setCurrentIndex(self.currentIndex()+1)
    
    def open_process_git(self):
        email, username = self.lineEdit_email.text(), self.lineEdit_username.text()
        link, path = self.lineEdit_link.text(), self.lineEdit_directory.text()
        commit = self.lineEdit_commit.text() or 'commmit'
        client = Client(path, link, email=email, username=username, commit=commit)
        thread = Thread(self, client)
        thread.logger.connect(self.logger)
        thread.stopped.connect(self.pushButton_start.setEnabled)
        thread.start()
        self.open_logger()
        
        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = App()
    MainWindow.show()
    sys.exit(app.exec_())
